'''
  Trie
  https://en.wikipedia.org/wiki/Trie

  >>> t = Trie()
  >>> t.insert('duck', 23)
  >>> t.find('duck')
  23
  >>> t.delete('duck')  # Return whether Trie is now empty
  True
  >>> t.insert('dump', 104)
  >>> t.insert('dude', 2045)
  >>> list(t.keys_with_prefix('du'))  # I miss the duck...*quack*
  ['dump', 'dude']
'''

class Trie(object):
    class Node(object):
        def __init__(self, value = None) -> None:
            self.children = {} # : Dict[str, Node] = {}
            self.value = value  # : Optional[Any] = None

    def __init__(self):
        self.root = self.Node()
        
    def insert(self, key, value):
        node = self.root
        for char in key:
            if char not in node.children:
                node.children[char] = self.Node()
            node = node.children[char]
        node.value = value
        
    def find(self, key : str):
        node = self._get_node(key)
        return node.value if node is not None else None

    def delete(self, key):
        return self._delete(self.root, key, 0)

    def keys_with_prefix(self, prefix):
        node = self._get_node(prefix)
        return self._collect(node, list(prefix))

    def _collect(self, node, prefix):
        if node is not None:
            if node.value is not None:
                prefix_str = ''.join(prefix)
                yield prefix_str
            for c in node.children:
                prefix.append(c)
                for p in self._collect(node.children[c], prefix):
                    yield p
                del prefix[-1]
                
    def _get_node(self, key : str):
        node = self.root
        for char in key:
            if char in node.children:
                node = node.children[char]
            else:
                node = None
                break
        return node
    
    def _delete(self, node, key, d):
        if d == len(key):
            node.value = None
        else:
            c = key[d]
            if c in node.children and self._delete(node.children[c], key, d+1):
                del(node.children[c])
        return node.value is None and len(node.children) == 0
        
if __name__ == "__main__":
    import doctest
    doctest.testmod()  
