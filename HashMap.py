'''
  HashMap
  https://en.wikipedia.org/wiki/Hash_table

  >>> h = HashMap(10)
  >>> h.has('not there?')
  False
  >>> h.get('not there?')
  Traceback (most recent call last):
  ...
  Exception: Failed to find key: not there?
  >>> h.set('duck', 'mango')
  >>> h.count()
  1
  >>> h.get('duck')
  'mango'
  >>> h.has('duck')
  True
  >>> h.set(27, 'howdy')
  >>> h.set(36.4, 'another')
  >>> h.count()
  3
  >>> h.remove(27)
  'howdy'
  >>> h.count()
  2
  >>> for slot in h:
  ...   print(slot)
  (36.4, 'another')
  ('duck', 'mango')
'''


class HashMap(object):
    DEFAULT_SIZE = 100
    MAX_SIZE = 1000
    def __init__(self, size = DEFAULT_SIZE):
        self.buckets = []
        self._resize(size)

    def has(self, key):
        bucket, i, slot = self._get(key)
        return i != -1
    
    def get(self, key):
        bucket, i, slot = self._get(key)
        
        if i == -1:
            raise Exception("Failed to find key: %s" % str(key))

        return slot[1] # (k, v)

    def set(self, key, value):
        bucket, i, slot = self._get(key)
        
        if i == -1:
            bucket.append((key, value))
        else:
            bucket[i] = (key, value)
            
    def remove(self, key):
        bucket, i, slot = self._get(key)
        if i != -1:
            bucket.remove(slot)
            
        return slot[1] # (k, v)
        
    def count(self):
        size = 0
        for bucket in self.buckets:
            size += len(bucket)
        return size

    def __iter__(self):
        for bucket in self.buckets:
            for slot in bucket:
                yield slot

        
    def _get(self, key):
        buckets = self.buckets
        
        i = hash(key) % len(buckets)
        
        bucket = buckets[i]
        found = False
        for i, slot in enumerate(buckets[i]):
            k, v = slot
            if key == k:
                found = True
                break
        if not found:
            i, slot = -1, None
            
        return (bucket, i, slot)
    
    def _resize(self, size = None):
        old_size = len(self.buckets)        
        if size is None:
            size = old_size * 2
        assert(size > 0)
        assert(size < self.MAX_SIZE)

        assert(size > old_size)
        old_buckets = self.buckets
        self.buckets = [[] for i in range(size)]

        for bucket in old_buckets:
            for k, v in bucket:
                set(key, value)
        
        
if __name__ == "__main__":
    import doctest
    doctest.testmod()
