'''
  Graph
  https://en.wikipedia.org/wiki/Graph_(abstract_data_type)

  >>> g = Graph()  # Default directed = False
  >>> g.add('first')
  >>> g.set('first', 27)
  >>> g.get('first')
  27
  >>> g.count()
  1
  >>> g.add('monkey')
  >>> g.add_edge('first', 'monkey')
  >>> g.has_edge('first', 'monkey') # aka adjacent
  True
  >>> g.has_edge('monkey', 'first') # directed = False, so has both edges!
  True
  >>> g.set_edge_value('monkey', 'first', 2045)
  >>> g.get_edge_value('monkey', 'first')
  2045
  >>> g.edge_count()
  1
  >>> g.remove_edge('monkey', 'first')
  >>> g.edge_count()
  0
  >>> g.count()
  2
  >>> g.add('banana')
  >>> g.add('chocolate')
  >>> g.add_edge('monkey', 'banana')
  >>> g.add_edge('monkey', 'chocolate')
  >>> g.edge_count()
  2

  Now for the really hard one...removing a vertex, which has an edge!
  >>> g.remove('monkey')  # Returns count of edges removed as well...
  2
  >>> g.edge_count()
  0
  >>> g.clear()
  >>> g.count()
  0
  >>> g.add('monkey')
  >>> g.add('banana')
  >>> g.add_edge('monkey', 'banana')
  >>> list(g.neighbors('monkey'))
  ['banana']
  >>> list(g.neighbors('banana'))
  ['monkey']
'''

class Graph(object):
    def __init__(self, directed = False):
        self.clear()
        self.directed = directed

    def clear(self):
        # { vertex_name: value }
        self.vertices = {}
        # { directed ? (vn1, vn2) : sorted((vn1, vn2)): value }
        self.edges = {}
        
    def add(self, name):
        if name in self.vertices:
            raise Exception('Vertex %s already exists' % str(name))
        self.vertices[name] = None

    def has(self, name):
        return name in self.vertices
    
    def set(self, name, value):
        assert(name in self.vertices)
        self.vertices[name] = value

    def get(self, name):
        assert(name in self.vertices)
        return self.vertices[name]

    def remove(self, name):
        assert(name in self.vertices)
        del(self.vertices[name])
        deleting = []
        for edge in self.edges:
            if name in edge:
                deleting.append(edge)
        for edge in deleting:
            del(self.edges[edge])
        return len(deleting)
    
    def count(self):
        return len(self.vertices)

    def edge_count(self):
        return len(self.edges)
    
    def add_edge(self, fr, to):
        key = self._key(fr, to)
        assert(key not in self.edges)
        self.edges[key] = None

    def has_edge(self, fr, to):
        return self._key(fr, to) in self.edges

    def remove_edge(self, fr, to):
        del(self.edges[self._key(fr, to)])
    
    def get_edge_value(self, fr, to):
        key = self._key(fr, to)
        assert(key in self.edges)
        return self.edges[key]

    def set_edge_value(self, fr, to, value):
        key = self._key(fr, to)
        assert(key in self.edges)
        self.edges[key] = value

    def neighbors(self, name):
        assert(name in self.vertices)
        found = []
        for fr, to in self.edges:
            if name == fr:
                yield to
            elif name == to:
                yield fr
    
    def _key(self, fr, to):
        key = [fr, to]
        if not self.directed:
            key.sort()
        return tuple(key)
    
if __name__ == "__main__":
    import doctest
    doctest.testmod()  

