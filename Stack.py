'''
  Stack
  https://en.wikipedia.org/wiki/Stack_(abstract_data_type)

  >>> s = Stack()
  >>> s.push(27)
  >>> s.push('duck')
  >>> s.pop()
  'duck'
  >>> s.count()
  1
  >>> s.push(3.14)
  >>> list(s)
  [3.14, 27]
  >>> s.pop()
  3.14
  >>> s.pop()
  27
  >>> s.pop()
  Traceback (most recent call last):
  ...
  Exception: popped empty stack
'''

class Stack(object):
    class Node(object):
        def __init__(self, v, next = None):
            self.v = v
            self.next = next

    def __init__(self):
        self.head = None

    def __iter__(self):
        n = self.head
        while n != None:
            yield n.v
            n = n.next
    
    def push(self, v):
        self.head = self.Node(v, self.head)

    def pop(self):
        if self.head is None:
            raise Exception('popped empty stack')
        v = self.head.v
        self.head = self.head.next
        return v

    def count(self):
        i = 0
        n = self.head
        while n != None:
            i += 1
            n = n.next
        return i

    
if __name__ == "__main__":
    import doctest
    doctest.testmod()
