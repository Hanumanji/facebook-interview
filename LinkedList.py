'''
  Linked List
  https://en.wikipedia.org/wiki/Linked_list

  >>> l = LinkedList()
  >>> l.add(17)
  0
  >>> l.count()
  1
  >>> l.add(49)
  1
  >>> l.count()
  2
  >>> l.get(1)
  49
  >>> l.get(0)
  17
  >>> l.add('duck')
  2
  >>> l.count()
  3
  >>> l.remove(1)
  49
  >>> l.count()
  2
  >>> list(l)
  [17, 'duck']
  >>> l.remove(1)
  'duck'
  >>> l.remove(0)
  17
  >>> l.count()
  0
  >>> l.add('happy?')
  0
'''

class LinkedList(object):
    class Node(object):
        def __init__(self, v):
            self.v = v
            self.next = None
            
    def __init__(self):
        self.head = None

    def __iter__(self):
        n = self.head
        while n is not None:
            yield n.v
            n = n.next
            
    def add(self, v):
        i = 0
        node = self.Node(v)
        if self.head is None:
            self.head = node
        else:
            i = 1
            n = self.head
            while n.next is not None:
                n = n.next
                i += 1
            n.next = node
            
        return i

    def get(self, i):
        assert(i >= 0)
        assert(i < self.count())
        n = self.head
        while i > 0:
            n = n.next
            i -= 1
        return n.v

    def remove(self, i):
        assert(i >= 0)
        assert(i < self.count())
        p = None
        n = self.head
        while i > 0:
            p = n
            n = p.next
            i -= 1

        if p is None:
            self.head = None
        else:
            p.next = n.next
            
        return n.v
        
    def count(self):
        n = self.head
        c = 0
        while n is not None:
            n = n.next
            c += 1
        return c
            
if __name__ == "__main__":
    import doctest
    doctest.testmod()
