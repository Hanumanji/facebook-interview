'''
  >>> findMaxProduct([1,2,3,4,5])
  [-1, -1, 6, 24, 60]
'''


def findMaxProduct(arr):
  # Write your code here
  from bisect import insort
  from functools import reduce
  # Thank goodness that it's 3 largest in 0..i
  # So can just keep 3 while we traverse looking for the max
  m = [] # Store the maximums here
  r = [] # Result
  for v in arr:
    insort(m, v)
    m = m[-3:]
    
    if len(m) < 3:
      r.append(-1)
    else:
      r.append(reduce(lambda x,y: x * y, m, 1))
  return r

if __name__ == "__main__":
    import doctest
    doctest.testmod()  
