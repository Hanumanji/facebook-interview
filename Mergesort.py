#! /usr/bin/python

def mergesort(a, lo = None, hi = None):
  """  Returns sorted copy of array

  >>> mergesort([ 7, 3, 4, -1, 8, 0, 5, 9, 2])
  [-1, 0, 2, 3, 4, 5, 7, 8, 9]
  """
  assert(isinstance(a, list))
  lo = 0 if lo is None else lo
  hi = len(a) if hi is None else hi

  r = a
  if hi - lo > 1:
    mid = (hi + lo) // 2
    # splits = a[lo:mid], a[mid:hi]
    # r = merge(*map(mergesort, splits))
    r = merge(mergesort(a[lo:mid]), mergesort(a[mid:hi]))
    
  return r
  
def merge(a, b):
  la = len(a)
  lb = len(b)
  l = la + lb
  r = [0] * l

  i = 0
  j = 0
  k = 0
  while k < l:
    va = a[i] if i < la else None
    vb = b[j] if j < lb else None

    if va is None or (vb is not None and vb < va):
      r[k] = vb
      j += 1
    elif vb is None or va < vb:
      r[k] = va
      i += 1
    
    k += 1
  
  return r

def m(a, b):
  ai, bi = iter(a), iter(b)
  try:
    av = next(ai)
  except StopIteration:
    yield from bi
    return
  try:
    bv = next(bi)
  except StopIteration:
    yield from ai
    return

  while True:
    if av < bv:
      yield av
      try:
        av = next(ai)
      except StopIteration:
        yield bv
        yield from bi
        return
    else:
      yield bv
      try:
        bv = next(bi)
      except StopIteration:
        yield av
        yield from ai
        return
      
  
def ms(a):
  """
  >>> list(ms([ 7, 3, 4, -1, 8, 0, 5, 9, 2]))
  [-1, 0, 2, 3, 4, 5, 7, 8, 9]
  """
  # m(ms([7,3,4,-1]),ms([8,0,5,9,2]))
  # m(m(ms([7,3]),ms([4,-1])),m(ms([8,0]),ms([5,9,2])))
  # m(m(m(ms([7]),ms([3])),m(ms([4]),ms([-1]))),m(m(ms([8]),ms([0])),m(ms([5]),ms([9,2]))))
  # m(m(m(ms([7]),ms([3])),m(ms([4]),ms([-1]))),m(m(ms([8]),ms([0])),m(ms([5]),m(ms([9]),ms([2])))))
  # m(m(m([7],[3])),m(ms([4]),ms([-1]))),m(m(ms([8]),ms([0])),m(ms([5]),m(ms([9]),ms([2])))))
  
  if len(a) == 1:
    return a
  
  mid = len(a) // 2
  return m(ms(a[:mid]),ms(a[mid:]))
  
if __name__ == "__main__":
    import doctest
    doctest.testmod()
    
