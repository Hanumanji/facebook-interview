'''
  >>> getTotalTime([4, 2, 1, 3])
  26

  >>> getTotalTime([2, 3, 9, 8, 4])
  88
'''

def maxPairIndices(arr):
    '''
    >>> maxPairIndices([2, 3, 9, 8, 4])
    (2, 3)
    '''
    from bisect import insort
    mv = []
    mi = {}

    for i, v in enumerate(arr):
        insort(mv, v)
        mi[v] = i
        if len(mv) > 2:
            sm = mv[0]
            mv = mv[-2:]
            del mi[sm]

    return tuple(mi.values())

def getTotalTime(arr):
    # Strangely, these lines actually crash!
    # m, a = getTotalTimeMax(arr), getTotalTimeAll(arr)
    # assert(m == a)
    # return m # or a...

    # While neither of these do
    # return getTotalTimeMax(arr)
    return getTotalTimeAll(arr)

def getTotalTimeMax(arr):
    s = 0
    if len(arr) > 1:
        (i, j) = maxPairIndices(arr)
        new_arr, new_s = penalty(arr, i, j)
        s = new_s + getTotalTimeMax(new_arr)
        
    return s


def getTotalTimeAll(arr):
    max_s = 0
    if len(arr) == 1:
        return 0
    else:
        for (i, j) in permsIter(arr):
            new_arr, s = penalty(arr, i, j)
            new_s = s + getTotalTime(new_arr)
            if new_s >= max_s:
                max_s = new_s
                  
    return max_s

def permsIter(arr):
    '''
    >>> list(permsIter([1,2,3]))
    [(0, 1), (0, 2), (1, 2)]
    '''
    l = len(arr) - 1
    for i in range(0, l):
        for j in range(i + 1, l + 1):
            yield (i, j)

def countPerms(arr):
    '''
    >>> countPerms([1,2,3])
    3
    '''    
    i = 0
    for _ in arr:
        i += 1
    return i

def penalty(arr, i, j):
  '''
    >>> penalty([1, 3, 5], 0, 1)
    ([5, 4], 4)
    >>> penalty([1, 2, 3, 4, 5, 6, 7], 2, 5)
    ([1, 2, 4, 5, 7, 9], 9)
  '''
  r = []
  s = 0
  for k, v in enumerate(arr):
    if k is not i and k is not j:
      r.append(v)
    else:
      s += v
  r.append(s)
  return r, s
    

if __name__ == "__main__":
    import doctest
    doctest.testmod()  
