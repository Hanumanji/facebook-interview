'''  Longest Zig Zag Sequence 
(From https://www.hiredintech.com/classrooms/algorithm-design/lesson/78)

  >>> lzz([])
  0
  >>> lzz([1])
  1
  >>> lzz([1, 3])
  2
  >>> lzz([4, 2, 1, 3, 3, 2, 3, 3])
  3
  >>> lzz([4, 2, 1, 3])
  3
  >>> lzz([2, 3, 9, 8, 4])
  3
  >>> lzz([-1, 1, -1, 1, 2, 1, 2, 3, 4, 3])
  4
  >>> lzz([1, -1, 1, -1, 1, 2, 1, 2, 3, 4, 3])
  5
  >>> lzz([1, -1, 1, -1, 1, 13, -400, 1, -1, 1, -1, 1, -1, 1, -1, 1, -1, 1, 7, 8, 9, 1, -1, 1, -1, 1, -1, 1,])
  14
  >>> lzz(alt(10))
  10
  >>> lzz(alt(32, -45))
  32
  >>> next(alt(3))
  1
'''

def alt(n, v = 1):
    for i in range(n):
        yield(v)
        v = -v

def lzz(a): # any iterable
    # Could special case 0, 1, & 2 values, saving 2*N compares for None
    # ...need to figure out how to do so works on iterators as well...
    
    ml = 0  # max length
    cl = 0  # current length
    pv = None # previous value
    hi = None # Was previous high or low?
    for v in a:  # for value in array
        if pv is None:  # No first value
            hi = None
            pv = v
            cl = 1
        elif v == pv: # Same as previous value, so...restart with one value
            hi = None
            pv = v
            cl = 1
        elif hi is None: # No second value
            hi = v > pv
            pv = v
            cl = 2
        elif hi == (v < pv): # Alternating properly
            hi = not hi
            pv = v
            cl += 1
        else: # Didn't alternate properly, so restart with two values
            hi = v > pv
            pv = v
            cl = 2

        if cl > ml:
            ml = cl

    return ml

if __name__ == "__main__":
    import doctest
    doctest.testmod()  
